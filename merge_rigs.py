import bpy

# Deselect all object
bpy.ops.object.select_all(action='DESELECT')

# Select all armatures
for ob in bpy.context.view_layer.objects:
    if ob.type == 'ARMATURE':
        ob.select_set(True)

# Make the armature called "Genesis 8 Female" the active object
rig = bpy.data.objects.get("Genesis 8 Female")
if rig and rig.type == 'ARMATURE':
    bpy.context.view_layer.objects.active = rig

    # Merge rigs
    bpy.ops.daz.merge_rigs(useSubrigsOnly = True)
else:
    print("Rig not found")