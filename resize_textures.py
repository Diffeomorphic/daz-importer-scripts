#
#   How to make textures local and resize them using a script.
#

import os
import bpy
try:
    from import_daz import api
except ModuleNotFoundError:
    from bl_ext.user_default.import_daz import api

#
#   Save local textures. The blend file must be saved first.
#

ob = bpy.context.object
if not (ob and ob.type == 'MESH'):
    raise RuntimeError("The active object must be a mesh")
if not bpy.data.filepath:
    raise RuntimeError("Save the blend file first")
bpy.ops.daz.save_local_textures()

#
#   Resize all image files by a factor 3
#

def getAllPaths(texpath, paths):
    for file in os.listdir(texpath):
        path = os.path.join(texpath, file)
        if os.path.splitext(file)[-1] in [".png", ".jpg", ".tif", ".bmp"]:
            paths.append(path)
        elif os.path.isdir(file):
            subpath = os.path.join(texpath, file)
            getAllPaths(subpath, paths)

folder = os.path.dirname(bpy.data.filepath)
texpath = os.path.join(folder, "textures")
paths = []
getAllPaths(texpath, paths)
api.set_selection(paths)
bpy.ops.daz.resize_textures(steps=3)

