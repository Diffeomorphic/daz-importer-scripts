#
#   How to load morphs and transfer shapekeys from a python script.
#   Updated for version 4.2.1
#
#   Root path, change to fit your system

rootpath = "D:\\DAZ 3D\\Studio\\My Library"

import os
import bpy
try:
    from import_daz import api
except ModuleNotFoundError:
    from bl_ext.user_default.import_daz import api

# With the character rig or mesh active, we next import three morphs that close
# the eyes.
# Use referencees relative to the DAZ root paths

unitsfolder = "/data/DAZ%203D/Genesis%208/Female/Morphs/DAZ%203D/Base%20Pose%20Head/"
files = ["eCTRLEyesClosed.dsf", "eCTRLEyesClosedL.dsf", "eCTRLEyesClosedR.dsf"]
refs = [unitsfolder+file for file in files]
abspaths = api.get_absolute_paths(refs)
print("Abspaths: %s" % abspaths)
api.set_selection(abspaths)
print("Import Units")
bpy.ops.daz.import_units()

# Get the morph ids loaded by the previous operator.
# The return value is a dict with entries
# Canonical filepath : Property name

units = api.get_return_value()
print("Units loaded:")
for path,prop in units.items():
    print("  %s: %s" % (path, prop))

# If we have invoked the operator with the 'INVOKE_DEFAULT' argument, it would
# instead have launced a morph selector in the same way as the Import Units
# button does.
#
# bpy.ops.daz.import_units('INVOKE_DEFAULT')
#
# Next we import two custom morph.
# Use full windows paths with \ separator

folder = "data\\DAZ 3D\\Genesis 8\\Female\\Morphs\\DAZ 3D\\Aiko 8"
files = ["FBMAiko8.dsf", "FHMAiko8.dsf"]
paths = [os.path.join(rootpath, folder, file) for file in files]
api.set_selection(paths)
print("Import Aiko")
bpy.ops.daz.import_custom_morphs(category = "Aiko")

# Two more custom morph.
# Use full windows paths with \ separator, but invoke get_absolute_paths anyway
# Daz instead of DAZ


folder = "data\\Daz 3D\\Genesis 8\\Female\\Morphs\\DAZ 3D\\Victoria 8"
files = ["FBMVictoria8.dsf", "FHMVictoria8.dsf"]
paths = [os.path.join(rootpath, folder, file) for file in files]
api.set_selection(paths)
print("Import Victoria")
bpy.ops.daz.import_custom_morphs(category = "Victoria")

# Note how the eyes have moved relative to the eyelashes. Loading morphs can be
# of limited use since Blender shapekeys only affect the mesh, whereas Daz morphs
# can affect the rig rest pose as well. To fix this we transfer the two Aiko morphs
# to the eyelashes.

body = bpy.data.objects["Genesis 8 Female Mesh"]
bpy.context.view_layer.objects.active = body
lashes = bpy.data.objects["Genesis 8 Female Eyelashes"]
lashes.select_set(True)
api.set_selection(["FBMAiko8", "FHMAiko8", "FBMVictoria8", "FHMVictoria8"])
print("Transfer shapekeys")
bpy.ops.daz.transfer_shapekeys(useDrivers=True)

# Then we import some standard JCMs
# Use paths relative to the root paths

folder = "/data/DAZ 3D/Genesis 8/Female/Morphs/DAZ 3D/Base Correctives/"
files = ["pJCMAbdomen2Fwd_40.dsf", "pJCMAbdomenFwd_35.dsf"]
relpaths = [folder+file for file in files]
abspaths = api.get_absolute_paths(relpaths)
api.set_selection(abspaths)
bpy.ops.daz.import_jcms()

# Set morphs by setting the rig properties,
# and force update to make the properties take

rig = bpy.data.objects["Genesis 8 Female"]
rig["FBMAiko8"] = 1.0
rig["FHMAiko8"] = 1.0
bpy.ops.daz.update_all()

# Set value of one slider from its path.
# The path must be in canonical form.

path = unitsfolder + "eCTRLEyesClosed.dsf"
print("Path: %s" % path)
canonical = api.get_canonical_filepath(path)
print("Canonical path: %s" % canonical)
prop = units.get(canonical)
if prop:
    print("Prop: %s" % prop)
    api.set_slider(rig, prop, 0.1)

# Set values of other units sliders, where the id and name differ,

api.set_slider(rig, "eCTRLEyesClosedL", 0.3)
api.set_slider(rig, "ECTRLEyesClosedR", 0.7)

# Update with function instead
api.update_drivers(rig)

