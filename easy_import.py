#
#   Easy import Aiko using a script, and then pose her and set an expression.
#   Updated for version 4.2.1
#
import os
import bpy
try:
    from import_daz import api
except ModuleNotFoundError:
    from bl_ext.user_default.import_daz import api

relpath = "/scenes/aiko-basic-wear.duf"
abspaths = api.get_absolute_paths([relpath])
print("Absolute paths: %s" % abspaths)
api.set_selection(abspaths)

# Turn on silent mode
api.set_silent_mode(True)

bpy.ops.daz.easy_import_daz(
   useExpressions = True,
   useJcms = True,
   clothesColor = (0,0,1,1.000),
   useTransferClothes = True,
   useMakePosable = True,
)
print("Loaded %s" % api.get_selection())


# Load a pose
relpath = "/people/genesis 8 female/poses/base poses/base pose kneeling b.duf"
abspaths = api.get_absolute_paths([relpath])
api.set_selection(abspaths)
bpy.ops.daz.import_pose()
print("Loaded %s" % api.get_selection())

# Set a morph
rig = bpy.context.object
rig["eCTRLSmileFullFace"] = 0.8
# Need an update
api.update_drivers(rig)

# Turn silent mode off again
api.set_silent_mode(False)
