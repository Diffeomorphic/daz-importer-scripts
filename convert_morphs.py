import bpy
try:
    from import_daz import api
except ModuleNotFoundError:
    from bl_ext.user_default.import_daz import api

bpy.ops.daz.update_slider_limits(min=3, max=5)
bpy.ops.daz.convert_morphs_to_shapekeys()
