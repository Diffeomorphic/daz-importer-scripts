#
#   Load a DAZ file and check its content.
#
try:
    from import_daz import api
except ModuleNotFoundError:
    from bl_ext.user_default.import_daz import api

def get_file_urls(relpath):
    abspaths = api.get_absolute_paths([relpath])
    if abspaths:
        abspath = abspaths[0]
    else:
        return "Not found", []
    struct = api.load_daz_file(abspath)
    ainfo = struct.get("asset_info", {})
    atype = ainfo.get("type", "Unknown")
    scene = struct.get("scene", {})
    urls = []
    for node in scene.get("nodes", []):
        url = node.get("url")
        if url:
            urls.append(url)
    return atype, urls


for filepath in [
    "/data/DAZ 3D/Genesis 8/Female 8_1/Genesis8_1Female.dsf",
    "/People/Genesis 8 Female/Characters/Aiko 8.duf",
    "/People/Genesis 3 Female/Poses/Fashion Model Pose 01.duf",
    "/People/Genesis 3 Female/Materials/Jeane Eyes 01.duf",
    "/People/Genesis 3 Female/Clothing/Basic Wear/Basic Wear.duf",
    ]:
    atype,urls = get_file_urls(filepath)
    print(
        "\nFile: %s\n" % filepath +
        "Asset type: %s" % atype)
    if urls:
        print("First URL: %s" % urls[0])




