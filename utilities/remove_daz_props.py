#
#   remove_daz_props
#   A utility program that removes all custom properties created by the DAZ Importer.
#
#   1. Disable the DAZ Importer
#   2. Run this script
#

import bpy

def remove_daz_props():
    def remove_props_from_rna(rna):
        for prop in list(rna.keys()):
            if keep_attributes and hasattr(rna, prop):
                pass
            elif prop.startswith("Daz"):
                print("DEL", prop)
                del rna[prop]

    if materials:
        for mat in bpy.data.materials:
            remove_props_from_rna(mat)
    if objects:
        for ob in bpy.data.objects:
            remove_props_from_rna(ob)
            if ob.type != 'EMPTY':
                remove_props_from_rna(ob.data)
            if ob.type == 'ARMATURE':
                for pb in ob.pose.bones:
                    remove_props_from_rna(pb)
                    remove_props_from_rna(pb.bone)
    if scenes:
        for scn in bpy.data.scenes:
            remove_props_from_rna(scn)


materials = True
objects = True
scenes = True
keep_attributes = True

remove_daz_props()
